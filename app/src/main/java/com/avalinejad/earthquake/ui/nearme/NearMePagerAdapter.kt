package com.avalinejad.earthquake.ui.nearme

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@ExperimentalCoroutinesApi
class NearMePagerAdapter(
    val context: Context,
    fm: FragmentManager,
    titles: List<String> = listOf()
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    var titles: List<String> = titles
        set(value) {
            if (field == value) {
                notifyDataSetChanged()
                return
            }
            field = value
            notifyDataSetChanged()
        }


    override fun getItem(position: Int): Fragment {
        return EarthQuakesListFragment(context)
    }

    override fun getCount() = titles.size

    override fun getPageTitle(position: Int): String {
        return titles[titles.size - position - 1]
    }

}