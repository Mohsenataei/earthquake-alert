package com.avalinejad.earthquake.ui.map

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.avalinejad.earthquake.MobileNavigationDirections
import com.avalinejad.earthquake.R
import com.avalinejad.earthquake.base.BaseFragment
import com.avalinejad.earthquake.data.model.EarthQuake
import com.avalinejad.earthquake.extentions.saveLastKnownCoordinates
import com.avalinejad.earthquake.extentions.updateMap
import com.avalinejad.earthquake.util.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.toolbar_layout.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
class HomeFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private val DEFAULT_ZOOM = 15f
    private lateinit var mMap: GoogleMap
    private var mLocationPermissionsGranted = false
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var locations: List<EarthQuake> = listOf()
    private var isListLoaded = false
    private var isMapReady = false
    val gson = Gson()


    private val viewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(HomeViewModel::class.java)
    }


    private val TAG = "MapFragment"

    override fun onMapReady(map: GoogleMap?) {
        mMap = map!!
        isMapReady = true
        mMap.setOnMarkerClickListener(this)
        addAllMarkers(locations)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initMap(view)
        toolbar.title = getString(R.string.app_name)
        GPSUtil(requireContext()) { isGPSEnabled ->
            if (isGPSEnabled!!)
                getDeviceLocation()
        }.turnGPSOn()

        viewModel.locations.observe(viewLifecycleOwner, Observer {
            isListLoaded = true
            locations = it
            addAllMarkers(it)
        })

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private fun initMap(view: View) {

        val mapFragment =
            childFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    private fun getLocationPermission() {
        if (!mLocationPermissionsGranted) {
            if (ContextCompat.checkSelfPermission(
                    requireContext().applicationContext,
                    FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
                    requireContext().applicationContext,
                    COURSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mLocationPermissionsGranted = true
                getDeviceLocation()
            } else {
                requestPermissions(LOCATION_PERMISSIONS, LOCATION_PERMISSION_REQUEST_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
//                mLocationPermissionsGranted = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED
//                if (grantResults.size > 0) {
//                    for (result in grantResults) {
//                        if (result != PackageManager.PERMISSION_GRANTED) {
//                            mLocationPermissionsGranted = false
//                            return
//                        }
//                    }
//                    Log.d(TAG, "onRequestPermissionsResult: permission granted")
//                    mLocationPermissionsGranted = true
//                }
                mLocationPermissionsGranted = grantResults
                    .takeIf { it.isNotEmpty() }
                    ?.map { it == PackageManager.PERMISSION_GRANTED }
                    ?.firstOrNull { it.not() }
                    ?.let { false }
                    ?: true
                getDeviceLocation()
            }
        }
    }

    private fun getDeviceLocation() {
        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(requireContext())
        try {
            if (!mLocationPermissionsGranted) {
                getLocationPermission()
            } else {
                val location = fusedLocationClient.lastLocation

                location.addOnCompleteListener(
                    requireActivity()
                ) { task ->
                    if (task.isSuccessful && task.result != null) {
                        val currentLocation = task.result
                        requireActivity().saveLastKnownCoordinates(
                            currentLocation!!.latitude.toFloat(),
                            currentLocation.longitude.toFloat()
                        )
//                        moveCamera(
//                            LatLng(currentLocation.latitude, currentLocation.longitude),
//                            DEFAULT_ZOOM
//                        )
                        viewModel.getMapPins(
                            32.4279,
                            53.6880,
                            "map"
                        )
                        mMap.updateMap(
                            32.4279,
                            53.6880,
                            4.65f
                        )
                    }
                }
//                location.addOnSuccessListener { location ->
//                    if (location != null){
//                        val currentLocation = location
//                        moveCamera(
//                            LatLng(currentLocation.longitude, currentLocation.longitude),
//                            DEFAULT_ZOOM
//                        )
//                    }
//                }
            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }


    }

    private fun moveCamera(latLng: LatLng, zoom: Float) {
        Log.d(
            TAG,
            "moving cammera to new location ==> lat is ${latLng.latitude} and lon is ${latLng.longitude}"
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GPS_REQUEST) {
                getDeviceLocation()
            }
        }
    }

    private fun addAllMarkers(items: List<EarthQuake>) {
        if (isListLoaded && isMapReady) {
            items.forEach { earthQuake ->
                mMap.addMarker(MarkerOptions().position(LatLng(earthQuake.lat, earthQuake.lng)))
                    .let { marker ->
                        marker.tag = earthQuake.id
                    }
            }
        }
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        var selected: EarthQuake? = null

        for (location in locations) {
            if (marker?.tag == location.id) {
                goToDetail(gson.toJson(location))
                return true
            }
        }
        return false
    }

    private fun goToDetail(detail: String) {
        findNavController().navigate(
            MobileNavigationDirections.actionGoToEarthDetailFragment(detail)
        )
    }
}