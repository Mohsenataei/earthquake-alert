package com.avalinejad.earthquake.ui.nearme

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.avalinejad.earthquake.MobileNavigationDirections
import com.avalinejad.earthquake.R
import com.avalinejad.earthquake.base.BaseFragment
import com.avalinejad.earthquake.extentions.getLastKnownCoordinates
import com.avalinejad.earthquake.extentions.shareEarthQuake
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_earth_quakes_list.*
import kotlinx.android.synthetic.main.fragment_earth_quakes_list.listLading
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
class EarthQuakesListFragment(context: Context) : BaseFragment() {

    private val mContext = context
    val gson = Gson()
    private val viewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(NearmeViewModel::class.java)
    }


    private val adapter: NearmeRecyclerViewAdapter by lazy {
        NearmeRecyclerViewAdapter(
            requireContext(),
            eventBus,
            onSelect = { earthQuake ->
                val detail = gson.toJson(earthQuake)
                findNavController().navigate(
                    MobileNavigationDirections.actionGoToEarthDetailFragment(
                        detail
                    )
                )
            },
            onShareClicked = {
                shareEarthQuake(it)
            }
        )
    }

    private lateinit var lastKnownLocation: LatLng

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_earth_quakes_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lastKnownLocation = mContext.getLastKnownCoordinates()
        getDate()
        viewModel.newMeEarthQuakes.observe(viewLifecycleOwner, Observer {
            adapter.items = it.toMutableList()
        })

        viewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            listLading.isVisible = it
        })
        earthQuakeListRecycler.layoutManager = LinearLayoutManager(context)
        earthQuakeListRecycler.adapter = adapter
    }

    private fun getDate() {
        viewModel.getNearEarthQuakes(
            lastKnownLocation.latitude,
            lastKnownLocation.longitude
        )
    }


}