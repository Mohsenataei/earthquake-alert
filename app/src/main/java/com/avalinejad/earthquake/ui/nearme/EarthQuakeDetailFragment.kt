package com.avalinejad.earthquake.ui.nearme

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.avalinejad.earthquake.R
import com.avalinejad.earthquake.base.BaseFragment
import com.avalinejad.earthquake.data.model.EarthQuake
import com.avalinejad.earthquake.extentions.getLastKnownCoordinates
import com.avalinejad.earthquake.util.DataFormatter
import com.avalinejad.earthquake.util.DateConverter
import com.avalinejad.earthquake.util.kiloJoulesToMegaWatts
import com.avalinejad.earthquake.util.kiloWattToTntExplosion
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
    import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_earth_quake_detail.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@ExperimentalCoroutinesApi
@FlowPreview
class EarthQuakeDetailFragment : BaseFragment(), OnMapReadyCallback {
    private val DEFAULT_ZOOM = 15f
    private lateinit var mMap: GoogleMap
    private lateinit var earthQuake: EarthQuake


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_earth_quake_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.inflateMenu(R.menu.detail_menu)

        earthQuake = EarthQuakeDetailFragmentArgs.fromBundle(requireArguments()).detail.let {
            EarthQuake.fromStringJson(it)
        }.apply {

        }
        initView()
        initMap()
    }

    private fun initView() {

        toolbar.title = "فاصله با شما".plus(" ").plus(earthQuake.distanceToUser).plus(getString(R.string.kilometer))
        depthTxt.text = earthQuake.eventDepth.toString() + " کیلومتر "
        var time = ""
        var date = ""
        var tmp = ""
        val converter = DateConverter()
        val d = DataFormatter.getStandardDate(earthQuake.eventTime)
        converter.gregorianToPersian(d.year, d.month, d.day)

        earthQuake.eventTime.apply {
            date = subSequence(0, indexOf(",")).toString()
            tmp = subSequence(length - 2, length).toString()
            time = subSequence(length - 11, length - 3).toString()
        }
        tmp = if (tmp.equals("PM")) " بعد از ظهر " else " صبح "

        timeTxt.text = time.plus(" ").plus(tmp)

        dateTxt.text = converter.day.toString().plus(" ").plus(DataFormatter.getPersianMonth(converter.month))

        eventSizeTxt.text =
            earthQuake.eventSize.toString().plus(" ").plus(getString(R.string.richter))

        description.text =
            "این زمین لرزه  "
                .plus(earthQuake.eventSize)
                .plus(" ریشتری ساعت ")
                .plus(time)
                .plus(tmp)
                .plus(converter.day)
                .plus("  ")
                .plus(DataFormatter.getPersianMonth(converter.month))
                .plus(" در فاصله ")
                .plus(earthQuake.distance)
                .plus(" کیلومتری از شهر ")
                .plus(earthQuake.city)
                .plus(" از توابع استان ")
                .plus(earthQuake.province)
                .plus(" رخ داد. ")

        volcano.text =
            "نزدیک ترین آتشفشان به این زمین لرزه آتشفشان ".plus(
                earthQuake.nearestVolcano.substringBefore(
                    "("
                )
            )
                .plus(" می باشد. ")
        val e = "3.2*10^10 J (8.78 MWh / 7.56 tons of TNT)"


        earthQuake.energy.apply {
            val baseValue = substringBefore("J")
            energyTxt.text = "انرژی این زمین لرزه ".plus(baseValue).plus("ژول یا ")
                .plus(kiloJoulesToMegaWatts(baseValue).toInt())
                .plus(" کیلو وات ساعت ")
                .plus("و معادل انرژی ازاد شده از انفجار ")
                .plus(kiloWattToTntExplosion(baseValue))
                .plus(" تن مواد منفجره TNT")
                .plus(" بود.")
        }

        locationTxt.text = getString(R.string.location)
            .plus(" ")
            .plus(getString(R.string.earthquake))
            .plus(" : ")
            .plus(earthQuake.lng)
            .plus(" , ")
            .plus(earthQuake.lat)


        val temp = "Jul 7, 2020 1:32:00 AM"
        var datee: String? = null

        temp.apply {
            datee = subSequence(0, indexOf(",") + 1).toString().apply {

            }
        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onMapReady(map: GoogleMap?) {
        map.apply {
            val event = LatLng(earthQuake.lat, earthQuake.lng)
            mMap = this!!
            addMarker(MarkerOptions().position(event)).title = earthQuake.city
            addMarker(MarkerOptions().position(requireContext().getLastKnownCoordinates())).title = getString(R.string.you)
            val b = LatLngBounds.Builder()
            b.include(requireContext().getLastKnownCoordinates())
            b.include(event)
            val bounds = b.build()
            animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,50))
//            updateMap((middldPoint(requireContext().getLastKnownCoordinates(),
//                LatLng(event.latitude,event.longitude)
//            )))
        }
    }

    private fun middldPoint(p1:LatLng, p2:LatLng) =
       LatLng((p1.latitude + p2.latitude)/2,(p1.longitude+p2.longitude)/2)

    private fun initMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.detail_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    enum class TimeHelper {
        AM, PM
    }

}