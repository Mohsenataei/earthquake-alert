package com.avalinejad.earthquake.ui.nearme

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.avalinejad.earthquake.R
import com.avalinejad.earthquake.bus.EventBus
import com.avalinejad.earthquake.data.model.EarthQuake
import com.avalinejad.earthquake.extentions.updateMap
import com.avalinejad.earthquake.util.DataFormatter
import com.avalinejad.earthquake.util.DateConverter
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.earth_quake_details_row.view.*

class NearmeRecyclerViewAdapter(
    val context: Context,
    val eventBus: EventBus,
    items: MutableList<EarthQuake> = mutableListOf(),
    val onSelect: (EarthQuake) -> Unit,
    val onShareClicked: (EarthQuake) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), View.OnClickListener {

    var items: MutableList<EarthQuake> = items
        set(value) {
            if (value == field)
                return
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return EarthQuakeViewHolder(parent.inflate(R.layout.earth_quake_details_row), null)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as EarthQuakeViewHolder).bind(items[position], position)
    }

    protected fun ViewGroup.inflate(layoutId: Int): View {
        return LayoutInflater.from(context).inflate(
            layoutId,
            this,
            false
        )
    }

    inner class EarthQuakeViewHolder(itemView: View, val earthQuake: EarthQuake?) :
        RecyclerView.ViewHolder(itemView),
        OnMapReadyCallback {

        init {
            itemView.setOnClickListener(this@NearmeRecyclerViewAdapter)
            itemView.imgShareNews.setOnClickListener(this@NearmeRecyclerViewAdapter)
        }

        lateinit var currentMap: GoogleMap
        lateinit var map: MapView
        lateinit var latLng: LatLng

        fun bind(earthQuake: EarthQuake, position: Int) {
            val converter = DateConverter()
            val d = DataFormatter.getStandardDate(earthQuake.eventTime)
            converter.gregorianToPersian(d.year, d.month, d.day)


            itemView.tag = position
            itemView.imgShareNews?.tag = position
//            itemView.decriptionTxtView.text =
//                "زلزله ای به بزرگی " + earthQuake.eventSize + " " + context.getString(R.string.richter) +
//                        " در " + earthQuake.distanceToUser + " کیلومتری شما " + earthQuake.city + " را لرزاند"
            itemView.title.text =
                " زلزله ".plus(earthQuake.eventSize).plus(" " + context.getString(R.string.richter))

            itemView.body.text = "استان: "
                .plus(earthQuake.province)
                .plus("\n")
                .plus("شهر: ")
                .plus(earthQuake.city)
                .plus("\n")
                .plus("فاصله با شما: ")
                .plus(earthQuake.distanceToUser)
                .plus(context.getString(R.string.kilometer))

            var time = ""
            var date = ""
            var tmp = ""
            earthQuake.eventTime.apply {
                date = subSequence(0, indexOf(",")).toString()
                tmp = subSequence(length - 2, length).toString()
                time = subSequence(length - 11, length - 3).toString()
            }
            tmp = if (tmp.equals("PM")) " بعد از ظهر " else " صبح "

            itemView.txtTime.text = time.plus(" ").plus(tmp)
            val map = itemView.mapView
            map?.onCreate(null)
            map?.onResume()
            map.getMapAsync(this)
            latLng = LatLng(earthQuake.lat, earthQuake.lng)
//            itemView.setOnClickListener {
//                onSelect(earthQuake)
//            }

            itemView.txtDate.text = converter.day.toString().plus(" ").plus(DataFormatter.getPersianMonth(converter.month))

                Log.d(
                    "testingDate",
                    "${converter.day} and ${converter.month} and ${converter.year}"
                )

        }

        override fun onMapReady(map: GoogleMap?) {
            MapsInitializer.initialize(context)
            map?.apply {
                updateMap(latLng, 7f)
                addMarker(MarkerOptions().position(latLng))
                mapType = GoogleMap.MAP_TYPE_SATELLITE
                currentMap = this
            }
        }
    }

    override fun onClick(v: View?) {
        val position = v!!.tag.toString().toInt()
        val item = items[position]

        when (v.id) {
            R.id.detailsContainer -> onSelect(item)
            R.id.imgShareNews -> onShareClicked(item)
        }
    }

}