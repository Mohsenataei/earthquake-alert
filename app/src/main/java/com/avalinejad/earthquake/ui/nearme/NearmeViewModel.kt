package com.avalinejad.earthquake.ui.nearme

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.avalinejad.earthquake.base.BaseViewModel
import com.avalinejad.earthquake.bus.EventBus
import com.avalinejad.earthquake.data.model.EarthQuake
import com.avalinejad.earthquake.respository.NearmeRepository
import com.avalinejad.earthquake.util.Coroutines
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject
@ExperimentalCoroutinesApi
@FlowPreview
class NearmeViewModel @Inject constructor(
    val eventBus: EventBus,
    private val nearMeRepository: NearmeRepository
) :
    BaseViewModel(eventBus) {

    val newMeEarthQuakes = MutableLiveData<List<EarthQuake>>()
    val loadingVisibility = MutableLiveData<Boolean>(false)


    @SuppressLint("LogNotTimber")

    fun getNearEarthQuakes(lat: Double, lng: Double) {
        Coroutines.ioThenMain({
            nearMeRepository.getNearMeEvents(lat, lng, type = "near")
        }) {
            onComplete {
                newMeEarthQuakes.value = it
                Log.d("adapter","$it")
            }
            onExecute { showLoading() }
            finally { hideLoading() }
        }.also {
            addToUnsubscribe(it)
        }
    }
    private fun showLoading() {
        loadingVisibility.value = true
    }

    private fun hideLoading() {
        loadingVisibility.value = false
    }

//    private fun getAll(lat: Double, lng: Double) {
//        Coroutines.ioThenMain({
//            nearMeRepository.getNearMeEvents(lat, lng)
//        }) {
//            onComplete {
//                newMeEarthQuakes.value = it
//                Log.d("adapter","$it")
//            }
//            onExecute { showLoading() }
//            finally { hideLoading() }
//        }.also {
//            addToUnsubscribe(it)
//        }
//    }
}