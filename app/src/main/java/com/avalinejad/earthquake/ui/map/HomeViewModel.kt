package com.avalinejad.earthquake.ui.map

import androidx.lifecycle.MutableLiveData
import com.avalinejad.earthquake.App
import com.avalinejad.earthquake.base.BaseViewModel
import com.avalinejad.earthquake.bus.EventBus
import com.avalinejad.earthquake.data.model.EarthQuake
import com.avalinejad.earthquake.respository.HomeRepository
import com.avalinejad.earthquake.respository.NearmeRepository
import com.avalinejad.earthquake.util.Coroutines
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

@ExperimentalCoroutinesApi
@FlowPreview
class HomeViewModel @Inject constructor(
    val eventBus: EventBus,
    app: App,
    private val nearmeRepository: NearmeRepository
) : BaseViewModel(eventBus) {

    val locations = MutableLiveData<List<EarthQuake>> ()

    val loadingVisibility = MutableLiveData<Boolean>(false)



    fun getMapPins(lat: Double, lng: Double, type: String){
        Coroutines.ioThenMain(
            {
                nearmeRepository.getNearMeEvents(lat, lng, type)
            }
        ){
            onComplete { locations.value = it }
            onExecute { showLoading() }
            finally { hideLoading() }
        }
    }

    private fun showLoading() {
        loadingVisibility.value = true
    }

    private fun hideLoading() {
        loadingVisibility.value = false
    }
}