package com.avalinejad.earthquake.ui.nearme

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.avalinejad.earthquake.MobileNavigationDirections
import com.avalinejad.earthquake.R
import com.avalinejad.earthquake.base.BaseFragment
import com.avalinejad.earthquake.data.model.EarthQuake
import com.avalinejad.earthquake.extentions.getLastKnownCoordinates
import com.avalinejad.earthquake.extentions.shareEarthQuake
import com.avalinejad.earthquake.extentions.updateMap
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_near_me.*
import kotlinx.android.synthetic.main.fragment_near_me.earthQuakeListRecycler
import kotlinx.android.synthetic.main.fragment_near_me.listLading
import kotlinx.android.synthetic.main.toolbar_layout.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import java.util.*

@ExperimentalCoroutinesApi
@FlowPreview
class NearmeFragment : BaseFragment(), OnMapReadyCallback {

    private val mContext = context
    val gson = Gson()
    private lateinit var earthQuakes: List<EarthQuake>
    private lateinit var mMap: GoogleMap
    private var isMapReady: Boolean = false
    private var isListReady: Boolean = false
    private val viewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(NearmeViewModel::class.java)
    }

    private val adapter: NearmeRecyclerViewAdapter by lazy {
        NearmeRecyclerViewAdapter(
            requireContext(),
            eventBus,
            onSelect = { earthQuake ->
                val detail = gson.toJson(earthQuake)
                findNavController().navigate(
                    MobileNavigationDirections.actionGoToEarthDetailFragment(
                        detail
                    )
                )
            },
            onShareClicked = {
                shareEarthQuake(it)
            }
        )
    }

    private lateinit var lastKnownLocation: LatLng

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_near_me, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMap()
        toolbar.title = getString(R.string.near_me)
            .plus(" - ")
            .plus(getString(R.string.atmost_200))
            .plus(" ")
            .plus(getString(R.string.kilometer))

        lastKnownLocation = requireContext().getLastKnownCoordinates()

        getDate()
        viewModel.newMeEarthQuakes.observe(viewLifecycleOwner, Observer {items->
            adapter.items = items.toMutableList()
            earthQuakes = items
            isListReady = true
            setMarkers()
        })

        viewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            listLading.isVisible = it
        })
        earthQuakeListRecycler.layoutManager = LinearLayoutManager(context)
        earthQuakeListRecycler.adapter = adapter
    }

    private fun getDate() {
        viewModel.getNearEarthQuakes(
            lastKnownLocation.latitude,
            lastKnownLocation.longitude
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onMapReady(map: GoogleMap?) {

        isMapReady = true
        map?.apply {
            mMap = this
//            addMarker(MarkerOptions().position(lastKnownLocation))
            updateMap(lastKnownLocation, 6f)
            mapType = GoogleMap.MAP_TYPE_SATELLITE
            setMarkers()

        }
    }

    private fun initMap() {

        val mapFragment =
            childFragmentManager.findFragmentById(R.id.satellite_google_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    private fun setMarkers() {
        if (isMapReady && isListReady){
            earthQuakes.forEach {
                mMap.addMarker(MarkerOptions().position(LatLng(it.lat,it.lng))).title = it.city
            }
        }
    }
}