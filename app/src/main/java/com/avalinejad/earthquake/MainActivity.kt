package com.avalinejad.earthquake

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.avalinejad.earthquake.bus.EventBus
import com.avalinejad.earthquake.di.DaggerViewModelFactory
import com.avalinejad.earthquake.event.NetworkError
import com.avalinejad.earthquake.extentions.isNetworkAvailable
import com.avalinejad.earthquake.extentions.networkErrorDialog
import com.avalinejad.earthquake.ui.map.HomeViewModel
import com.avalinejad.earthquake.util.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject


const val TAG = "MainActivity"

@ExperimentalCoroutinesApi
@FlowPreview
class MainActivity : AppCompatActivity(), HasAndroidInjector, OnMapReadyCallback {


    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any?>
    override fun androidInjector(): AndroidInjector<Any?>? = androidInjector

    @Inject
    lateinit var eventBus: EventBus

    private var mLocationPermissionsGranted = false

    private lateinit var mMap: GoogleMap
    lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration


    private val viewModel by viewModels<HomeViewModel> { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        setContentView(R.layout.activity_main)
        forceRtlSupport()
        initEvents()
//        locationEnabled()
        getLocationPermission()
        initView()
    }

    @SuppressLint("LogNotTimber")
    private fun initEvents(){
        eventBus.collectEventOnMainThread<NetworkError> {
            if (isNetworkAvailable().not()) {
                networkErrorDialog("اینترنت را بررسی کنید", it.onRetry, it.onCancle)
            }
        }.also {
            Log.d(TAG,"network event invoked")
        }
    }

    private fun initView() {
        bottomNavigationView.selectedItemId = R.id.nav_search
        navController = findNavController(R.id.nav_host_fragment)
        bottomNavigationView.setupWithNavController(navController)
    }

    override fun onMapReady(map: GoogleMap?) {
        mMap = map!!
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)

        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(
                this.applicationContext,
                COURSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        )
            mLocationPermissionsGranted = true
        else
            ActivityCompat.requestPermissions(this, LOCATION_PERMISSIONS,
                LOCATION_PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        mLocationPermissionsGranted = false
        when(requestCode){
            LOCATION_PERMISSION_REQUEST_CODE -> {
                mLocationPermissionsGranted = grantResults
                    .takeIf { it.isNotEmpty() }
                    ?.map { it==PackageManager.PERMISSION_GRANTED }
                    ?.firstOrNull{it.not()}
                    ?.let { false }
                    ?:true
            }
        }
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun forceRtlSupport(){
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL;
    }

    fun goToSearchEarthQuakes(item: MenuItem) {
        navController.navigate(
            MobileNavigationDirections.goToSearch()
        )
    }

    fun getBack(item: MenuItem){
        navController.popBackStack()
    }

    private fun goToAllNews(title: String) {
        navController.navigate(
            MobileNavigationDirections.goToSearch()
        )
    }

    private fun locationEnabled(){
        val lm : LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        var gpsEnabled = false
        var networkEnabled = false
        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (!gpsEnabled && !networkEnabled) {
            AlertDialog.Builder(this@MainActivity)
                .setMessage("GPS Enable")
                .setPositiveButton("Settings",
                    DialogInterface.OnClickListener { paramDialogInterface, paramInt ->
                        startActivity(
                            Intent(ACTION_LOCATION_SOURCE_SETTINGS)
                        )
                    })
                .setNegativeButton("Cancel", null)
                .show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == GPS_REQUEST){

            }
        }
    }

//    override fun attachBaseContext(newBase: Context?) {
//        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase!!))
//    }

}
