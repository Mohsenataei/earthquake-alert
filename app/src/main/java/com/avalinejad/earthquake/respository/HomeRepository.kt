package com.avalinejad.earthquake.respository

import com.avalinejad.earthquake.data.remote.AppApiService
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val appApiService: AppApiService
){
    suspend fun getMapInfo() = appApiService
}