package com.avalinejad.earthquake.respository

import com.avalinejad.earthquake.App
import com.avalinejad.earthquake.data.remote.AppApiService
import javax.inject.Inject

class NearmeRepository @Inject constructor(
    private val app: App,
    private val apiService: AppApiService
) {
    suspend fun getNearMeEvents(lat: Double, lng: Double , type: String ) =
        apiService.getNearestEarthquakes(lat, lng, service = type).await()

}
