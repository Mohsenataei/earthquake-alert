package com.avalinejad.earthquake.event

class LoadingVisibility(val visible: Boolean)

class FinishActivityEvent(val classObject: Class<*>)

class NetworkError(val throwable: Throwable, val onRetry: (() -> Unit)? = null, val onCancle: (() -> Unit)? = null)
