package com.avalinejad.earthquake.di

import com.avalinejad.earthquake.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {
    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun ContributeChooserAtivity(): MainActivity

}