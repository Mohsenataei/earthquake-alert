package com.avalinejad.earthquake.di

import androidx.lifecycle.ViewModel
import com.avalinejad.earthquake.ui.map.HomeViewModel
import com.avalinejad.earthquake.ui.nearme.NearmeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@ExperimentalCoroutinesApi
@FlowPreview
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NearmeViewModel::class)
    abstract fun bindNearMeViewModel(viewModel: NearmeViewModel): ViewModel
}