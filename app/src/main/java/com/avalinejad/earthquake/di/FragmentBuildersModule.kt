package com.avalinejad.earthquake.di

import com.avalinejad.earthquake.ui.descriptions.DescriptionsFragment
import com.avalinejad.earthquake.ui.map.HomeFragment
import com.avalinejad.earthquake.ui.nearme.EarthQuakeDetailFragment
import com.avalinejad.earthquake.ui.nearme.EarthQuakesListFragment
import com.avalinejad.earthquake.ui.nearme.NearmeFragment
import com.avalinejad.earthquake.ui.search.SearchFragment
import com.avalinejad.earthquake.ui.settings.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@FlowPreview
@ExperimentalCoroutinesApi
@Module
abstract class FragmentBuildersModule {

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeMapFragment(): HomeFragment


    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeSettingsFragment(): SettingsFragment

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeSearchFragment(): SearchFragment

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeNearmeFragment(): NearmeFragment

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeDescriptionFragment(): DescriptionsFragment


    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeEarthQuakesListFragment(): EarthQuakesListFragment

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeEarthQuakesDetailFragment(): EarthQuakeDetailFragment




}