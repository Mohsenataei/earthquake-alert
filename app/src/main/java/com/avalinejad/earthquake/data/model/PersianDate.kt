package com.avalinejad.earthquake.data.model

data class PersianDate(
    val day : Int,
    val month: Int,
    val year: Int
)