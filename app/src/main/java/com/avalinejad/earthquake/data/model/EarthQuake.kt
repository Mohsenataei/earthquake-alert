package com.avalinejad.earthquake.data.model

import com.avalinejad.earthquake.util.GsonFactory
import com.google.gson.annotations.SerializedName

data class EarthQuake(
    @SerializedName("id")
    val id: Long,
    @SerializedName("distanceToUser")
    val distanceToUser: Long,
    val distance: Long,
    @SerializedName("eventSize")
    val eventSize: Double,
    @SerializedName("nearestVolcano")
    val nearestVolcano: String,
    val province: String,
    val lng: Double,
    @SerializedName("eventTime")
    val eventTime: String,
    val energy: String,
    val lat: Double,
    val city: String,
    @SerializedName("eventDepth")
    val eventDepth: Int,
    val title: String
) {
    companion object {
        fun fromStringJson(earthQuake: String): EarthQuake =
            GsonFactory.instance.singletonGson.fromJson(
                earthQuake,
                EarthQuake::class.java
            )
    }
}