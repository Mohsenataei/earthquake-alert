package com.avalinejad.earthquake.data.remote

import com.avalinejad.earthquake.data.model.EarthQuake
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface AppApiService {


    @GET("eq.jsp")
    fun getNearestEarthquakes(
        @Query("lat") lat: Double?,
        @Query("lng") lng: Double?,
        @Query("distance") distance: Int? = 200,
        @Query("min") min: Int? = 2,
        @Query("s") service: String = "near"

    ):Deferred<List<EarthQuake>>






}