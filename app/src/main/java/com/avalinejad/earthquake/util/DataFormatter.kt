package com.avalinejad.earthquake.util

import java.util.*

class DataFormatter() {

    companion object {
        var day = ""
        var month = ""
        var year = ""

        // "Jul 7, 2020 10:10:00 PM"
        fun getStandardDate(date: String): Date {
            date.apply {
                month = substring(0, 3)
                day = substring(4, indexOf(","))
                year = substring(indexOf(",") + 2, indexOf(",") + 6)
            }
            return Date(year.toInt(), getMonth(month), day.toInt())
        }

        fun getMonth(month: String) =
            when (month) {
                "Jan" -> 1
                "Feb" -> 2
                "Mar" -> 3
                "Apr" -> 4
                "May" -> 5
                "Jun" -> 6
                "Jul" -> 7
                "Aug" -> 8
                "Sep" -> 9
                "Oct" -> 10
                "Nov" -> 11
                "Dec" -> 12
                else -> 0
            }

        fun getPersianMonth(month: Int) =
            when (month) {
                1 -> "فروردین"
                2 -> "اردیبهشت"
                3 -> "خرداد"
                4 -> "تیر"
                5 ->"مرداد"
                6 ->"شهریور"
                7 ->"مهر"
                8 -> "آبان"
                9 -> "آذر"
                10 -> "دی"
                11 -> "بهمن"
                12 ->"اسفند"
                else -> throw Exception("month not defined.")
            }
    }

}