package com.avalinejad.earthquake.util

import android.Manifest

val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
val LOCATION_PERMISSION_REQUEST_CODE = 1234
val GPS_REQUEST = 1111
val DEFAULT_ZOOM = 15f
val DETAIL_ZOOM = 7f

val LOCATION_PERMISSIONS = arrayOf(
   FINE_LOCATION,
    COURSE_LOCATION
)