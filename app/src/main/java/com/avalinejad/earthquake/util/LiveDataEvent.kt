package com.avalinejad.earthquake.util

open class LiveDataEvent<out T>(private val content: T, val callback: (() -> Unit)? = null, val onError: (() -> Unit)? = null) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    val validContent: T?
        get() {
            return if (hasBeenHandled) {
                null
            } else {
                hasBeenHandled = true
                content
            }
        }


    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}