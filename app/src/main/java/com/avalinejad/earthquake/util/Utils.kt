package com.avalinejad.earthquake.util

import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.pow

fun kiloJoulesToMegaWatts(joules: String): Double{
    val pow = joules.substringAfter("^").toDouble()
    val base = joules.substringBefore("*").toDouble()

    val convertedPow = pow -7
    val result = 10.0.pow(convertedPow)
    return base.times(result)
}

fun kiloWattToTntExplosion(joules: String) : BigDecimal{
    val pow = joules.substringAfter("^").toDouble()
    var base = joules.substringBefore("*").toDouble()

    val convertedPow = pow-9
    var res: Double? = null
    return if (convertedPow == 0.0){
        BigDecimal(base.div(4.184)).setScale(2,RoundingMode.HALF_EVEN)
    }else
    {
        res = 10.0.pow(convertedPow)
        base = base.div(4.184)
        BigDecimal(base.times(res)).setScale(2,RoundingMode.HALF_EVEN)
    }
}