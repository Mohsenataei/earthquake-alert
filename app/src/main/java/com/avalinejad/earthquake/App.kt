package com.avalinejad.earthquake

import com.avalinejad.earthquake.bus.EventBus
import com.avalinejad.earthquake.di.AppComponent
import com.avalinejad.earthquake.di.AppInjector
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber
import javax.inject.Inject

class App : DaggerApplication(){

    @Inject
    lateinit var eventBus: EventBus
    lateinit var appComponent: AppComponent
    override fun applicationInjector(): AndroidInjector<out DaggerApplication>  = appComponent

    override fun onCreate() {
        AppInjector.initInjector(this)
        super.onCreate()

//        ViewPump.init(ViewPump.builder()
//            .addInterceptor(CalligraphyInterceptor(
//                CalligraphyConfig.Builder()
//                    .setDefaultFontPath("fonts/yekan_bakh_fa_num_regular.ttf")
//                    .setFontAttrId(R.attr.fontPath)
//                    .build()
//            )).build()
//        )

        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())

        FirebaseAnalytics.getInstance(this).setSessionTimeoutDuration(1000)

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Timber.e( task.exception)
                return@addOnCompleteListener
            }
            Timber.e( "FirebaseToken----------------: ${task.result?.token}")
        }

    }

}