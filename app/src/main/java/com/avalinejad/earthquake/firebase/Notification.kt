package com.avalinejad.earthquake.firebase

import android.os.Parcelable
import androidx.core.app.NotificationCompat
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Notification(
    val title: String?,
    val body: String?,
    val color: String?,
    val sound: String?,
    val smallIcon: String?,
    val largeIcon: String?,
    val bigPicture: String?,
    val notificationChanel: String?,
    val priority: String?,
    val url: String?,
    val additionalData: Map<String,String>?,
    val ongoing: Boolean?,
    val target: String?
) : Parcelable {
    companion object {
        const val NOTIFICATION_KEY = "NOTIFICATION_KEY"
        const val LGT_DEFAULT_NOTIFICATION_CHANEL_ID = "LGT_DEFAULT_NOTIFICATION_CHANEL_ID"
    }
}

@Parcelize
data class KeyValue(
    val key: String,
    val value: String
) : Parcelable

fun getPriority(stringPriority: String?) = when (stringPriority?.toLowerCase()) {
    "min" -> NotificationCompat.PRIORITY_MIN
    "low" -> NotificationCompat.PRIORITY_LOW
    "high" -> NotificationCompat.PRIORITY_HIGH
    "max" -> NotificationCompat.PRIORITY_MAX
    else -> NotificationCompat.PRIORITY_DEFAULT
}