package com.avalinejad.earthquake.firebase

class ImageSize(val width: Int, val height: Int)
