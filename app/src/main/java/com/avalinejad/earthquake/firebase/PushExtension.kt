package com.avalinejad.earthquake.firebase

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.squareup.picasso.Picasso

fun Context.getResourceId(name: String) = resources.getIdentifier(name, "drawable", packageName)
    .takeIf { it != 0 }

fun Context.getBitmap(address: String, size: ImageSize? = null): Bitmap? {
    try {
        if (address.startsWith("http"))
            Picasso.get()
                .load(address)
                .apply {
                    (size ?: ImageSize(
                        256,
                        256
                    )).let {
                        resize(it.width, it.height)
                    }
                }.get()
        return getResourceId(address)?.let {
            BitmapFactory.decodeResource(resources, it)
        }
    } catch (e: Exception) {
    }
    return null
}