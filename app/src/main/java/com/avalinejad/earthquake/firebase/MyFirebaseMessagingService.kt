package com.avalinejad.earthquake.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.WindowManager
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.avalinejad.earthquake.MainActivity
import com.avalinejad.earthquake.R
import com.avalinejad.earthquake.extentions.makeOpenBrowserIntent
import com.avalinejad.earthquake.firebase.Notification.Companion.LGT_DEFAULT_NOTIFICATION_CHANEL_ID
import com.avalinejad.earthquake.util.GsonFactory
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlin.random.Random

class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        FirebaseMessaging.getInstance().subscribeToTopic("ALL_USERS")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.containsKey("notification").not())
            super.onMessageReceived(remoteMessage)
        else
            handleNotification(remoteMessage)
    }

    private fun handleNotification(remoteMessage: RemoteMessage) {
        Log.d("FCMService", "data: ${remoteMessage.data}")

        val stringNotification = remoteMessage.data["notification"] ?: return

        val notification = GsonFactory.instance.adapterlesSingletonGson.fromJson(
            stringNotification, Notification::class.java
        )


        val channelId = notification.notificationChanel ?: LGT_DEFAULT_NOTIFICATION_CHANEL_ID

        val builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(notification)
            .setLargeIcon(notification)
            .setTitle(notification)
            .setBody(notification)
            .setColor(notification)
            .setSound(notification)
            .setBigContent(notification)
            .setIntent(notification)
            .setOngoing(notification.ongoing ?: false)
            .setPriority(getPriority(notification.priority))
            .setAutoCancel(true)

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        createChanel(channelId, manager)
        manager.notify(Random.nextInt(), builder.build())
    }


    private fun NotificationCompat.Builder.setIntent(notification: Notification) = apply {
        val intent = notification.url?.takeIf { it.startsWith("http") }?.let {
            makeOpenBrowserIntent(it)
        } ?: run {
            Intent(this@MyFirebaseMessagingService, MainActivity::class.java)
        }
        setContentIntent(
            PendingIntent.getActivity(
                this@MyFirebaseMessagingService,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT
            )
        )
    }

    private fun createChanel(channelId: String, manager: NotificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Chanel_$channelId",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            manager.createNotificationChannel(channel)
        }
    }

    private fun NotificationCompat.Builder.setBigContent(notification: Notification) = apply {

        val bitmap = notification.bigPicture?.let {
            val width = getDeviceWidth()
            getBitmap(it, ImageSize(width, width / 2))
        }
        if (bitmap != null)
            setStyle(
                NotificationCompat.BigPictureStyle()
                    .bigPicture(bitmap)
                    .bigLargeIcon(null)
            )
        else
            setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(notification.body ?: getString(R.string.app_name))
            )
    }

    private fun getDeviceWidth(): Int {
        val window = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = window.defaultDisplay
        return display.width
    }


    private fun NotificationCompat.Builder.setSound(notification: Notification) = apply {
        val sound = notification.sound?.let {
            Uri.parse("android.resource://$packageName/$it")
        } ?: RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        setSound(sound)
    }

    private fun NotificationCompat.Builder.setColor(notification: Notification) = apply {
        val color = notification.color?.let { Color.parseColor(it) }
            ?: ContextCompat.getColor(this@MyFirebaseMessagingService, R.color.colorPrimaryDark)
        setColor(color)
    }

    private fun NotificationCompat.Builder.setTitle(notification: Notification) = apply {
        setContentTitle(notification.title ?: getString(R.string.app_name))
    }

    private fun NotificationCompat.Builder.setBody(notification: Notification) = apply {
        setContentText(notification.body ?: getString(R.string.app_name))
    }

    private fun NotificationCompat.Builder.setLargeIcon(notification: Notification) = apply {
        val largeIcon = notification.largeIcon?.let { getBitmap(it) }
            ?: BitmapFactory.decodeResource(resources, R.drawable.ic_launcher)
        setLargeIcon(largeIcon)
    }

    private fun NotificationCompat.Builder.setSmallIcon(notification: Notification) = apply {
        val smallIcon = notification.smallIcon?.let { getResourceId(it) }
            ?: R.drawable.ic_launcher
        setSmallIcon(smallIcon)
    }


    companion object {
        const val TAG = "MyFirebaseMsgService"
        const val NOTIFICATION_OPENED_ACTION = "com.faraji.lgt.NOTIFICATION_OPENED_ACTION"

    }
}