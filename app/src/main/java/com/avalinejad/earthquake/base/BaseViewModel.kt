package com.avalinejad.earthquake.base

import androidx.lifecycle.ViewModel
import com.avalinejad.earthquake.bus.EventBus
import com.avalinejad.earthquake.extentions.clearAllDisposables
import kotlinx.coroutines.Job
import javax.inject.Inject

open class BaseViewModel @Inject constructor(
    private val eventBus: EventBus
): ViewModel(){


    private val disposableJob by lazy { mutableListOf<Job>() }

    fun addToUnsubscribe(job: Job){
        disposableJob.add(job)
    }

    override fun onCleared() {
        disposableJob.clearAllDisposables()
    }

}