package com.avalinejad.earthquake.extentions

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.Settings
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import java.util.*
import kotlin.math.ln


const val COORDINATE_PREFERENCES = "coordinate"
const val LATITUDE = "latitude"
const val LONGITUDE = "longitude"


fun Context.getDeviceId(): String? {
    return try {
        Settings.Secure.getString(
            contentResolver,
            Settings.Secure.ANDROID_ID
        )
    } catch (e: java.lang.Exception) {
        "unknown_user_id_" + Date().time
    }
}

//fun Context.getSecrets(): Array<String?>? {
//    return resources.getStringArray(R.array.secrets)
//}


fun Context.isPackageExist(targetPackage: String) = try {
    packageManager.getPackageInfo(targetPackage, PackageManager.GET_META_DATA)
    true
} catch (e: PackageManager.NameNotFoundException) {
    false
}


fun Context.makeOpenBrowserIntent(url: String): Intent {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    val chromePackage = "com.android.chrome"
    if (isPackageExist(chromePackage)) {
        intent.setPackage(chromePackage)
    }
    return intent
}

fun Context.isNetworkAvailable(): Boolean {
    val connectivity = (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?)
    return connectivity?.activeNetworkInfo?.isConnected ?: false
}

fun Context.saveLastKnownCoordinates(lat: Float, lng: Float) =
    getSharedPreferences(COORDINATE_PREFERENCES, Context.MODE_PRIVATE).edit().let {
        it.putFloat(LATITUDE, lat)
        it.putFloat(LONGITUDE, lng)
        it.apply()
    }

fun Context.getLastKnownCoordinates() =
    getSharedPreferences(COORDINATE_PREFERENCES, Context.MODE_PRIVATE).let {
        LatLng(it.getFloat(LATITUDE, 0f).toDouble(), it.getFloat(LONGITUDE, 0f).toDouble())
    }


fun Context.getLatitude() = getSharedPreferences(COORDINATE_PREFERENCES, Context.MODE_PRIVATE)
    .getFloat(LATITUDE, 0f)


fun Context.getLongitude() = getSharedPreferences(COORDINATE_PREFERENCES, Context.MODE_PRIVATE)
    .getFloat(LONGITUDE, 0f)

