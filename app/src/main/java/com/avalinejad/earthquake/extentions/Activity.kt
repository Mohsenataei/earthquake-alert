package com.avalinejad.earthquake.extentions

import androidx.appcompat.app.AppCompatActivity
import com.avalinejad.earthquake.R
import com.avalinejad.earthquake.ui.dialogfragments.MessageDialogFragment


@Synchronized
fun AppCompatActivity.messageDialog(
    message: MessageDialogFragment.Message,
    tag: String? = null,
    cancelable: Boolean = true,
    onclick: (() -> Unit)? = null,
    onCancel: (() -> Unit)? = null
) = MessageDialogFragment.newInstance(message, cancelable, onclick, onCancel)
    .show(supportFragmentManager, tag ?: "MessageDialog")



fun AppCompatActivity.messageDialog(
    message: String,
    buttonText: String? = null,
    image: Int? = null,
    tag: String? = null,
    cancelable: Boolean = true,
    onClick: (() -> Unit)? = null,
    onCancel: (() -> Unit)? = null
) = messageDialog(
    MessageDialogFragment.Message(message, buttonText, image),
    tag,
    cancelable,
    onClick,
    onCancel
)


fun AppCompatActivity.networkErrorDialog(
    message: String,
    onRetry: (() -> Unit)? = null,
    onCancel: (() -> Unit)? = null
) {
    messageDialog(
        message,
        getString(R.string.retry),
        null,
        NETWORK_ERROR_DIALOG_TAG,
        true,
        onRetry, onCancel

    )
}