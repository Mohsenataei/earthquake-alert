package com.avalinejad.earthquake.extentions

import android.content.Intent
import androidx.fragment.app.Fragment
import com.avalinejad.earthquake.R
import com.avalinejad.earthquake.data.model.EarthQuake

const val NETWORK_ERROR_DIALOG_TAG = "NetworkError"

fun Fragment.shareText(message: String, subject: String = "") {
    val sendIntent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, message)
        putExtra(Intent.EXTRA_SUBJECT, subject)
        type = "text/plain"
    }
    startActivity(Intent.createChooser(sendIntent, resources.getText(R.string.send_to)))
}

fun Fragment.shareEarthQuake(earthQuakes: EarthQuake) =
    shareText(
        "زلزله ای به بزرگی "
            .plus(earthQuakes.eventSize)
            .plus(" ریشتر حوالی ")
            .plus(earthQuakes.city)
            .plus(" را لرزاند")
            .plus("\n\n\n")
            .plus("گزارش مقدماتی زمین لرزه :")
            .plus("\n\n")
            .plus("محل وقوع : استان ")
            .plus(earthQuakes.province)
            .plus(" - حوالی ")
            .plus(earthQuakes.city)
            .plus("\n تاریخ و زمان وقوع به وقت محلی : ")
            .plus("\n")
            .plus(earthQuakes.eventTime)
            .plus("\n طول جغرافیایی : ")
            .plus(earthQuakes.lat)
            .plus("\n عرض جغرافیایی : ")
            .plus(earthQuakes.lng)
            .plus("\n عمق زمین لرزه : ")
            .plus(earthQuakes.eventDepth),
        "هشدار زلزله"
    )

