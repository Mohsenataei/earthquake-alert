package com.avalinejad.earthquake.extentions

import com.avalinejad.earthquake.util.DEFAULT_ZOOM
import com.avalinejad.earthquake.util.DETAIL_ZOOM
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng

fun GoogleMap.updateMap(lat: Double, lng: Double){
    moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat,lng), DEFAULT_ZOOM))
}

fun GoogleMap.updateMap(latLng: LatLng){
    moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DETAIL_ZOOM))
}

fun GoogleMap.updateMap(lat: Double, lng: Double, zoom: Float){
    moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat,lng), zoom))
}

fun GoogleMap.updateMap(latLng: LatLng, zoom: Float){
    moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
}

